//
//  ViewController.swift
//  PostCards From Paradise
//
//  Created by Moisés Córdova on 12/21/17.
//  Copyright © 2017 Moisés Córdova. All rights reserved.
//

import UIKit
import MobileCoreServices

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDragDelegate, UIDropInteractionDelegate, UIDragInteractionDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initColors()
        self.colorCollectionView.dragDelegate = self
        self.postCardImageView.isUserInteractionEnabled = true
        let dropInteraction = UIDropInteraction(delegate: self)
        self.postCardImageView.addInteraction(dropInteraction)
        let dragInteracion = UIDragInteraction(delegate: self)
        self.postCardImageView.addInteraction(dragInteracion)
        self.title = "Postales desde el Paraiso"
        splitViewController?.view.backgroundColor = UIColor.lightGray
        renderPostcard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var postCardImageView: UIImageView!
    @IBOutlet weak var colorCollectionView: UICollectionView!
    
    var colors = [UIColor]()
    
    var image : UIImage?
    
    var topText = "Bienvenido"
    var bottomText = "Tus postales"
    
    var topFontName = "Avenir Next"
    var bottomFontName = "Avenir Next"
    
    var topFontColor = UIColor.white
    var bottomFontColor = UIColor.white
    
    func initColors(){
        self.colors += [.black, .gray, .white, .red, .orange, .yellow, .green, .cyan, .blue, .purple, .magenta]
        
        for hue in 0...9 {
            for sat in 1...10 {
                let color = UIColor(hue: CGFloat(hue)/10, saturation: CGFloat(sat)/10, brightness: 1, alpha: 1)
                self.colors.append(color)
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath)
        
        let color = self.colors[indexPath.row]
        cell.backgroundColor = color
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 5
        return cell
    }
    
    func renderPostcard(){
        let drawRect = CGRect(x: 0, y: 0, width: 3000, height: 2400)
        let topRect = CGRect(x: 300, y: 200, width: 2400, height: 800)
        let bottomRect = CGRect(x: 300, y: 1800, width: 2400, height: 600)
        let topFont = UIFont(name: self.topFontName, size: 250) ?? UIFont.systemFont(ofSize: 240)
        let bottomFont = UIFont(name: self.bottomFontName, size: 120) ?? UIFont.systemFont(ofSize: 80)
        let centered = NSMutableParagraphStyle()
        centered.alignment = .center
        
        let topAttributes : [NSAttributedStringKey : Any] =
            [
                .foregroundColor    : topFontColor,
                .font               : topFont,
                .paragraphStyle     : centered
        ]
        
        let bottomAttributes : [NSAttributedStringKey : Any] =
            [
                .foregroundColor    : topFontColor,
                .font               : bottomFont,
                .paragraphStyle     : centered
        ]
        let renderer = UIGraphicsImageRenderer(size: drawRect.size)
        self.postCardImageView.image = renderer.image(actions: { (context) in
            UIColor.lightGray.set()
            context.fill(drawRect)
            self.image?.draw(at: CGPoint(x: 0, y: 0))
            self.topText.draw(in: topRect, withAttributes: topAttributes)
            self.bottomText.draw(in: bottomRect, withAttributes: bottomAttributes)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let color = self.colors[indexPath.row]
        let itemProvider = NSItemProvider(object: color)
        let item = UIDragItem(itemProvider: itemProvider)
        
        return [item]
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        guard let image = self.postCardImageView.image else { return [] }
        let provider = NSItemProvider(object: image)
        let item = UIDragItem(itemProvider: provider)
        return [item]
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .copy)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        let dropLocation = session.location(in: self.postCardImageView)
        if session.hasItemsConforming(toTypeIdentifiers: [kUTTypePlainText as String]) {
            session.loadObjects(ofClass: NSString.self, completion: { items in
                guard let fontName = items.first as? String else { return }
                if dropLocation.y < self.postCardImageView.bounds.midY {
                    self.topFontName = fontName
                } else {
                    self.bottomFontName = fontName
                }
                self.renderPostcard()
            })
        } else if session.hasItemsConforming(toTypeIdentifiers: [kUTTypeImage as String]) {
            session.loadObjects(ofClass: UIImage.self, completion: { (items) in
                guard let image = items.first as? UIImage else { return }
                self.image = image
                self.renderPostcard()
            })
        } else {
            session.loadObjects(ofClass: UIColor.self, completion: { items in
                guard let color = items.first as? UIColor else { return }
                
                if dropLocation.y < self.postCardImageView.bounds.midY {
                    self.topFontColor = color
                } else {
                    self.bottomFontColor = color
                }
                self.renderPostcard()
            })
        }
    }
    
    @IBAction func changeText(_ sender: UITapGestureRecognizer) {
        let tapLocation = sender.location(in: self.postCardImageView)
        
        let changeTop = (tapLocation.y < self.postCardImageView.bounds.midY) ? true : false
        
        let alert = UIAlertController(title: "Cambiar texto", message: "Escribe el nuevo texto", preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = "¿Qué quieres poner aquí?"
            
            if changeTop {
                textField.text = self.topText
            } else {
                textField.text = self.bottomText
            }
        }
        
        let changeAction = UIAlertAction(title: "Cambiar Texto", style: .default) { _ in
            guard let newText = alert.textFields?[0].text else { return }
            
            if changeTop {
                self.topText = newText
            } else {
                self.bottomText = newText
            }
            self.renderPostcard()
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel)
        alert.addAction(changeAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
    }
}
